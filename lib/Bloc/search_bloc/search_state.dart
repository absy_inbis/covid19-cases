abstract class SearchState {}

class Initial extends SearchState {}

class Searched extends SearchState {
  Searched(this.text);

  final String text;
}